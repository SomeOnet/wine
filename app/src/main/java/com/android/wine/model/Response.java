package com.android.wine.model;

public class Response {
    private String message;
    private String token;

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }
}
