package com.android.wine.network;


import android.util.Base64;

import com.android.wine.utils.Constants;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public class NetworkUtil {
    private static RetrofitInterface retrofitInterface;

    static volatile Retrofit retrofit = null;

    private NetworkUtil(){}

    public static Retrofit getRetrofit(){
        if (retrofit == null){
            synchronized (NetworkUtil.class){
                if (retrofit == null){
                    RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
                    OkHttpClient okHttpClient = new OkHttpClient();
                    retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addCallAdapterFactory(rxAdapter)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(okHttpClient)
                            .build();
                }
            }
        }
        return retrofit;
    }

    public static Retrofit getRetrofit(String email, String password){
        if (retrofit == null) {
            synchronized (NetworkUtil.class) {
                if (retrofit == null) {
                    String credentials = email + ":" + password;
                    String basic = "Basic"+ Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    OkHttpClient.Builder http = new OkHttpClient.Builder();
                    http.addInterceptor(chain -> {
                        Request original = chain.request();
                        Request.Builder builder = original.newBuilder()
                                .addHeader("Authorization", basic)
                                .method(original.method(), original.body());
                        return chain.proceed(builder.build());
                    });
                    RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
                    retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addCallAdapterFactory(rxAdapter)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(http.build())
                            .build();
                }
            }
        }
        return retrofit;
    }

    public static Retrofit getRetrofit(String token){
        if (retrofit == null) {
            synchronized (NetworkUtil.class) {
                if (retrofit == null) {
                    OkHttpClient.Builder http = new OkHttpClient.Builder();
                    http.addInterceptor(chain -> {
                        Request original = chain.request();
                        Request.Builder builder = original.newBuilder()
                                .addHeader("x-access-token", token)
                                .method(original.method(), original.body());
                        return chain.proceed(builder.build());
                    });
                    RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
                    retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addCallAdapterFactory(rxAdapter)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(http.build())
                            .build();
                }
            }
        }
        return retrofit;
    }

    public static void initApiService() {
        if (retrofitInterface == null) {
            synchronized (NetworkUtil.class) {
                if (retrofitInterface == null) {
                    retrofitInterface = getRetrofit().create(RetrofitInterface.class);
                }
            }
        }
    }

    public static RetrofitInterface getPostApi() {
        initApiService();
        return retrofitInterface;
    }
}
