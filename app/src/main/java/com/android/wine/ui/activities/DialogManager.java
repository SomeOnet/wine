package com.android.wine.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.wine.R;


public class DialogManager extends AppCompatActivity {
    protected ProgressDialog progressDialog;

    private static DialogManager dialogManagerInstance;

    private DialogManager(){
    }

    public static DialogManager getInstance(){
        if (dialogManagerInstance == null){
            dialogManagerInstance = new DialogManager();
        }
        return dialogManagerInstance;
    }

    public void showProgress() {
        if (progressDialog == null || !progressDialog.isShowing()){
            progressDialog = new ProgressDialog(this, R.style.custom_dialog);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
            progressDialog.setContentView(R.layout.progress_splash);
        }else {
            progressDialog.show();
            progressDialog.setContentView(R.layout.progress_splash);
        }
    }

    public void hideProgress() {
        if (progressDialog != null){
            if (progressDialog.isShowing()){
                progressDialog.hide();
            }
        }
    }

    public void showError(String message, Exception error) {
        showToast(message);
    }

    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG);
    }


}
