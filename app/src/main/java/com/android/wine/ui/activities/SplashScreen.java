package com.android.wine.ui.activities;

import android.app.Activity;

import com.android.wine.MainActivity_;
import com.android.wine.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_splash_screen)
public class SplashScreen extends Activity {

    @AfterViews
    void init(){
        intent();
    }

    @Background(id = "start", delay = 2600)
    void intent(){
        MainActivity_.intent(this).start();
    }
}
