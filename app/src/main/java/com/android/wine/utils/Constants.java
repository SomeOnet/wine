package com.android.wine.utils;

/**
 * Created by Moziskiyy on 12/8/2016.
 */

public class Constants {
    public static final String BASE_URL = "http://127.0.0.1:8080/api/v1";
    public static final String TOKEN = "token";
    public static final String EMAIL = "email";
}
